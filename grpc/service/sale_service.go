package service

import (
	"app/config"
	"app/genproto/kassa_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SaleService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*kassa_service.UnimplementedSaleServiceServer
}

func NewSaleService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SaleService {
	return &SaleService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *SaleService) Create(ctx context.Context, req *kassa_service.CreateSale) (resp *kassa_service.Sale, err error) {

	i.log.Info("---CreateSale------>", logger.Any("req", req))

	pKey, err := i.strg.Sale().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSale->Sale->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Sale().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeySale->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleService) GetByID(ctx context.Context, req *kassa_service.SalePrimaryKey) (resp *kassa_service.Sale, err error) {

	i.log.Info("---GetSaleByID------>", logger.Any("req", req))

	resp, err = i.strg.Sale().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSaleByID->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleService) GetList(ctx context.Context, req *kassa_service.GetListSaleRequest) (resp *kassa_service.GetListSaleResponse, err error) {

	i.log.Info("---GetSales------>", logger.Any("req", req))

	resp, err = i.strg.Sale().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSales->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleService) Update(ctx context.Context, req *kassa_service.UpdateSale) (resp *kassa_service.Sale, err error) {

	i.log.Info("---UpdateSale------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Sale().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSale--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Sale().GetByPKey(ctx, &kassa_service.SalePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSale->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SaleService) UpdatePatch(ctx context.Context, req *kassa_service.UpdatePatchSale) (resp *kassa_service.Sale, err error) {

	i.log.Info("---UpdatePatchSalePayment------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Sale().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchSale--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Sale().GetByPKey(ctx, &kassa_service.SalePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSale->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SaleService) Delete(ctx context.Context, req *kassa_service.SalePrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteSale------>", logger.Any("req", req))

	err = i.strg.Sale().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSale->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
