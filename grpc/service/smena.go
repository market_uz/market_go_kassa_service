package service

import (
	"app/config"
	"app/genproto/kassa_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SmenaService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*kassa_service.UnimplementedSmenaServiceServer
}

func NewSmenaService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SmenaService {
	return &SmenaService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *SmenaService) Create(ctx context.Context, req *kassa_service.CreateSmena) (resp *kassa_service.Smena, err error) {

	i.log.Info("---CreateSmena------>", logger.Any("req", req))

	pKey, err := i.strg.Smena().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSmena->Smena->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Smena().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeySmena->Smena->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SmenaService) GetByID(ctx context.Context, req *kassa_service.SmenaPrimaryKey) (resp *kassa_service.Smena, err error) {

	i.log.Info("---GetSmenaByID------>", logger.Any("req", req))

	resp, err = i.strg.Smena().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSmenaByID->Smena->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SmenaService) GetList(ctx context.Context, req *kassa_service.GetListSmenaRequest) (resp *kassa_service.GetListSmenaResponse, err error) {

	i.log.Info("---GetSmenas------>", logger.Any("req", req))

	resp, err = i.strg.Smena().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSmenas->Smena->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SmenaService) Update(ctx context.Context, req *kassa_service.UpdateSmena) (resp *kassa_service.Smena, err error) {

	i.log.Info("---UpdateSmena------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Smena().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSmena--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Smena().GetByPKey(ctx, &kassa_service.SmenaPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSmena->Smena->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SmenaService) UpdatePatch(ctx context.Context, req *kassa_service.UpdatePatchSmena) (resp *kassa_service.Smena, err error) {

	i.log.Info("---UpdatePatchSmenaPayment------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Smena().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchSmena--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Smena().GetByPKey(ctx, &kassa_service.SmenaPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSmena->Smena->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SmenaService) Delete(ctx context.Context, req *kassa_service.SmenaPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteSmena------>", logger.Any("req", req))

	err = i.strg.Smena().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSmena->Smena->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
