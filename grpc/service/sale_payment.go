package service

import (
	"app/config"
	"app/genproto/kassa_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SalePaymentService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*kassa_service.UnimplementedSalePaymentServiceServer
}

func NewSalePaymentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SalePaymentService {
	return &SalePaymentService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *SalePaymentService) Create(ctx context.Context, req *kassa_service.CreateSalePayment) (resp *kassa_service.SalePayment, err error) {

	i.log.Info("---CreateSalePayment------>", logger.Any("req", req))

	pKey, err := i.strg.SalePayment().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSalePayment->SalePayment->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.SalePayment().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeySalePayment->SalePayment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SalePaymentService) GetByID(ctx context.Context, req *kassa_service.SalePaymentPrimaryKey) (resp *kassa_service.SalePayment, err error) {

	i.log.Info("---GetSalePaymentByID------>", logger.Any("req", req))

	resp, err = i.strg.SalePayment().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSalePaymentByID->SalePayment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SalePaymentService) GetList(ctx context.Context, req *kassa_service.GetListSalePaymentRequest) (resp *kassa_service.GetListSalePaymentResponse, err error) {

	i.log.Info("---GetSalePayments------>", logger.Any("req", req))

	resp, err = i.strg.SalePayment().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSalePayments->SalePayment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SalePaymentService) Update(ctx context.Context, req *kassa_service.UpdateSalePayment) (resp *kassa_service.SalePayment, err error) {

	i.log.Info("---UpdateSalePayment------>", logger.Any("req", req))

	rowsAffected, err := i.strg.SalePayment().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSalePayment--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.SalePayment().GetByPKey(ctx, &kassa_service.SalePaymentPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSale->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SalePaymentService) UpdatePatch(ctx context.Context, req *kassa_service.UpdatePatchSalePayment) (resp *kassa_service.SalePayment, err error) {

	i.log.Info("---UpdatePatchSalePayment------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.SalePayment().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchSalePayment--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.SalePayment().GetByPKey(ctx, &kassa_service.SalePaymentPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSalePayment->SalePayment->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SalePaymentService) Delete(ctx context.Context, req *kassa_service.SalePaymentPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteSalePayment------>", logger.Any("req", req))

	err = i.strg.SalePayment().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSalePayment->SalePayment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
