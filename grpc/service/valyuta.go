package service

import (
	"app/config"
	"app/genproto/kassa_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ValyutaService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*kassa_service.UnimplementedValyutaServiceServer
}

func NewValyutaService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ValyutaService {
	return &ValyutaService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ValyutaService) AddCurrency() error {
	i.log.Info("---AddCurrency")

	err := i.strg.Valyuta().AddFromApi()
	if err != nil {
		i.log.Error("!!!AddCurrency->Currency->Add--->", logger.Error(err))
		return status.Error(codes.InvalidArgument, err.Error())
	}
	
	return nil
}

func (i *ValyutaService) Create(ctx context.Context, req *kassa_service.CreateValyuta) (resp *kassa_service.Valyuta, err error) {

	i.log.Info("---CreateValyuta------>", logger.Any("req", req))

	pKey, err := i.strg.Valyuta().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateValyuta->Valyuta->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Valyuta().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyValyuta->Valyuta->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ValyutaService) GetByID(ctx context.Context, req *kassa_service.ValyutaPrimaryKey) (resp *kassa_service.Valyuta, err error) {

	i.log.Info("---GetValyutaByID------>", logger.Any("req", req))

	resp, err = i.strg.Valyuta().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetValyutaByID->Valyuta->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ValyutaService) GetList(ctx context.Context, req *kassa_service.GetListValyutaRequest) (resp *kassa_service.GetListValyutaResponse, err error) {

	i.log.Info("---GetValyutas------>", logger.Any("req", req))

	resp, err = i.strg.Valyuta().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetValyutas->Valyuta->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ValyutaService) Update(ctx context.Context, req *kassa_service.UpdateValyuta) (resp *kassa_service.Valyuta, err error) {

	i.log.Info("---UpdateValyuta------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Valyuta().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateValyuta--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Valyuta().GetByPKey(ctx, &kassa_service.ValyutaPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetValyuta->Valyuta->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ValyutaService) UpdatePatch(ctx context.Context, req *kassa_service.UpdatePatchValyuta) (resp *kassa_service.Valyuta, err error) {

	i.log.Info("---UpdatePatchValyutaPayment------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Valyuta().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchValyuta--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Valyuta().GetByPKey(ctx, &kassa_service.ValyutaPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetValyuta->Valyuta->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ValyutaService) Delete(ctx context.Context, req *kassa_service.ValyutaPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteValyuta------>", logger.Any("req", req))

	err = i.strg.Valyuta().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteValyuta->Valyuta->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
