package storage

import (
	"app/genproto/kassa_service"
	"app/models"
	"context"
)

type StorageI interface {
	CloseDB()
	Sale() SaleRepoI
	SaleProduct() SaleProductRepoI
	SalePayment() SalePaymentRepoI
	Smena() SmenaRepoI
	Transaction() TransactionRepoI
	Valyuta() ValyutaRepoI
}

type SaleRepoI interface {
	Create(ctx context.Context, req *kassa_service.CreateSale) (resp *kassa_service.SalePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *kassa_service.SalePrimaryKey) (resp *kassa_service.Sale, err error)
	GetAll(ctx context.Context, req *kassa_service.GetListSaleRequest) (resp *kassa_service.GetListSaleResponse, err error)
	Update(ctx context.Context, req *kassa_service.UpdateSale) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *kassa_service.SalePrimaryKey) error
}

type SaleProductRepoI interface {
	Create(ctx context.Context, req *kassa_service.CreateSaleProduct) (resp *kassa_service.SaleProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *kassa_service.SaleProductPrimaryKey) (resp *kassa_service.SaleProduct, err error)
	GetAll(ctx context.Context, req *kassa_service.GetListSaleProductRequest) (resp *kassa_service.GetListSaleProductResponse, err error)
	Update(ctx context.Context, req *kassa_service.UpdateSaleProduct) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *kassa_service.SaleProductPrimaryKey) error
}

type SalePaymentRepoI interface {
	Create(ctx context.Context, req *kassa_service.CreateSalePayment) (resp *kassa_service.SalePaymentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *kassa_service.SalePaymentPrimaryKey) (resp *kassa_service.SalePayment, err error)
	GetAll(ctx context.Context, req *kassa_service.GetListSalePaymentRequest) (resp *kassa_service.GetListSalePaymentResponse, err error)
	Update(ctx context.Context, req *kassa_service.UpdateSalePayment) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *kassa_service.SalePaymentPrimaryKey) error
}

type TransactionRepoI interface {
	Create(ctx context.Context, req *kassa_service.CreateTransaction) (resp *kassa_service.TransactionPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *kassa_service.TransactionPrimaryKey) (resp *kassa_service.Transaction, err error)
	GetAll(ctx context.Context, req *kassa_service.GetListTransactionRequest) (resp *kassa_service.GetListTransactionResponse, err error)
	Update(ctx context.Context, req *kassa_service.UpdateTransaction) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *kassa_service.TransactionPrimaryKey) error
}

type SmenaRepoI interface {
	Create(ctx context.Context, req *kassa_service.CreateSmena) (resp *kassa_service.SmenaPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *kassa_service.SmenaPrimaryKey) (resp *kassa_service.Smena, err error)
	GetAll(ctx context.Context, req *kassa_service.GetListSmenaRequest) (resp *kassa_service.GetListSmenaResponse, err error)
	Update(ctx context.Context, req *kassa_service.UpdateSmena) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *kassa_service.SmenaPrimaryKey) error
}

type ValyutaRepoI interface {
	AddFromApi() error
	
	Create(ctx context.Context, req *kassa_service.CreateValyuta) (resp *kassa_service.ValyutaPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *kassa_service.ValyutaPrimaryKey) (resp *kassa_service.Valyuta, err error)
	GetAll(ctx context.Context, req *kassa_service.GetListValyutaRequest) (resp *kassa_service.GetListValyutaResponse, err error)
	Update(ctx context.Context, req *kassa_service.UpdateValyuta) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *kassa_service.ValyutaPrimaryKey) error
}
