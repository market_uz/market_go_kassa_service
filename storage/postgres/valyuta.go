package postgres

import (
	"app/genproto/kassa_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type valyutaRepo struct {
	db *pgxpool.Pool
}

type Currency struct {
	Id        int    `json:"dd"`
	Code      string `json:"code"`
	Ccy       string `json:"ccy"`
	CcyNm_RU  string `json:"ccyNm_RU"`
	CcyNm_UZ  string `json:"ccyNm_UZ"`
	CcyNm_UZC string `json:"ccyNm_UZC"`
	CcyNm_EN  string `json:"ccyNm_EN"`
	Nominal   string `json:"nominal"`
	Rate      string `json:"rate"`
	Diff      string `json:"diff"`
	Date      string `json:"date"`
}

func NewValyutaRepo(db *pgxpool.Pool) *valyutaRepo {
	return &valyutaRepo{
		db: db,
	}
}

func (c *valyutaRepo) AddFromApi() error {
	query := `DELETE FROM "currency"`

	_, err := c.db.Exec(context.Background(), query)
	if err != nil {
		return err
	}

	resp, err := http.Get("https://cbu.uz/oz/arkhiv-kursov-valyut/json/")
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var data []Currency
	err = json.Unmarshal(body, &data)
	if err != nil {
		return err
	}

	for _, currency := range data {
		id := uuid.New().String()

		query = `
			INSERT INTO "currency_data" (
				id,
				name,
				abreviatura,
			) VALUES ($1, $2, $3)
		`
		_, err := c.db.Exec(context.Background(), id, currency.CcyNm_EN, currency.Ccy)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *valyutaRepo) Create(ctx context.Context, req *kassa_service.CreateValyuta) (resp *kassa_service.ValyutaPrimaryKey, err error) {
	var id = uuid.New().String()

	query := `
		INSERT INTO "currency_data" (
			id,
			name,
			abreviatura,
			updated_at
		) VALUES ($1, $2, $3, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.Name,
		req.Abreviatura,
	)
	if err != nil {
		return nil, err
	}

	return &kassa_service.ValyutaPrimaryKey{Id: id}, nil
}

func (c *valyutaRepo) GetByPKey(ctx context.Context, req *kassa_service.ValyutaPrimaryKey) (resp *kassa_service.Valyuta, err error) {
	query := `
		SELECT
			id,
			name,
			abreviatura,
			price,
			date,
			created_at,
			updated_at
		FROM "currency_data"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		name        sql.NullString
		abreviatura sql.NullString
		price       sql.NullFloat64
		date        sql.NullString
		created_at  sql.NullString
		updated_at  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&abreviatura,
		&price,
		&date,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &kassa_service.Valyuta{
		Id:          id.String,
		Name:        name.String,
		Abreviatura: abreviatura.String,
		Price:       price.Float64,
		Date:        date.String,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}

	return
}

func (c *valyutaRepo) GetAll(ctx context.Context, req *kassa_service.GetListValyutaRequest) (resp *kassa_service.GetListValyutaResponse, err error) {
	resp = &kassa_service.GetListValyutaResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			SELECT
			id,
			name,
			abreviatura,
			price,
			date,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "transactions"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			abreviatura sql.NullString
			price       sql.NullFloat64
			date        sql.NullString
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&abreviatura,
			&price,
			&date,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.Valyutas = append(resp.Valyutas, &kassa_service.Valyuta{
			Id:          id.String,
			Name:        name.String,
			Abreviatura: abreviatura.String,
			Price:       price.Float64,
			Date:        date.String,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}

	return
}

func (c *valyutaRepo) Update(ctx context.Context, req *kassa_service.UpdateValyuta) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"currency_data"
		SET
			abreviatura = :abreviatura,
			price = :price,
			updated_at = now()
		WHERE name = :name
	`

	params = map[string]interface{}{
		"name":        req.GetName(),
		"abreviatura": req.GetAbreviatura(),
		"price":       req.GetPrice(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *valyutaRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"currency_data"
		` + set + ` , updated_at = NOW()
			WHERE id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *valyutaRepo) Delete(ctx context.Context, req *kassa_service.ValyutaPrimaryKey) error {
	query := `DELETE FROM "currency_data" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
