package postgres

import (
	"app/genproto/kassa_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type saleRepo struct {
	db *pgxpool.Pool
}

func NewSaleRepo(db *pgxpool.Pool) *saleRepo {
	return &saleRepo{
		db: db,
	}
}

func (c *saleRepo) Create(ctx context.Context, req *kassa_service.CreateSale) (resp *kassa_service.SalePrimaryKey, err error) {
	var id = uuid.New().String()

	id_nomer := helper.GenerateCode()

	query := `
		INSERT INTO "sales" (
			id, 
			scan_barcode,
			id_nomer,
			branch_id,
			shop_id,
			employee_id,
			updated_at
		) VALUES ($1, $2, $3, $4, $5, $6, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.ScanBarcode,
		id_nomer,
		req.BranchId,
		req.ShopId,
		req.EmployeeId,
	)
	if err != nil {
		return nil, err
	}

	return &kassa_service.SalePrimaryKey{Id: id}, nil
}

func (c *saleRepo) GetByPKey(ctx context.Context, req *kassa_service.SalePrimaryKey) (resp *kassa_service.Sale, err error) {
	query := `
		SELECT
			id,
			scan_barcode,
			id_nomer,
			branch_id,
			shop_id,
			employee_id,
			status,
			created_at,
			updated_at
		FROM "sales"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		scan_barcode sql.NullString
		id_nomer     sql.NullString
		branch_id    sql.NullString
		shop_id      sql.NullString
		employee_id  sql.NullString
		status       sql.NullInt32
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&scan_barcode,
		&id_nomer,
		&branch_id,
		&shop_id,
		&employee_id,
		&status,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &kassa_service.Sale{
		Id:          id.String,
		ScanBarcode: scan_barcode.String,
		IdNomer:     id_nomer.String,
		BranchId:    branch_id.String,
		ShopId:      shop_id.String,
		EmployeeId:  employee_id.String,
		Status:      status.Int32,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}

	return
}

func (c *saleRepo) GetAll(ctx context.Context, req *kassa_service.GetListSaleRequest) (resp *kassa_service.GetListSaleResponse, err error) {
	resp = &kassa_service.GetListSaleResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			scan_barcode,
			id_nomer,
			branch_id,
			shop_id,
			employee_id,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sales"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id           sql.NullString
			scan_barcode sql.NullString
			id_nomer     sql.NullString
			branch_id    sql.NullString
			shop_id      sql.NullString
			employee_id  sql.NullString
			status       sql.NullInt32
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&scan_barcode,
			&id_nomer,
			&branch_id,
			&shop_id,
			&employee_id,
			&status,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.Sales = append(resp.Sales, &kassa_service.Sale{
			Id:          id.String,
			ScanBarcode: scan_barcode.String,
			IdNomer:     id_nomer.String,
			BranchId:    branch_id.String,
			ShopId:      shop_id.String,
			EmployeeId:  employee_id.String,
			Status:      status.Int32,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}

	return
}

func (c *saleRepo) Update(ctx context.Context, req *kassa_service.UpdateSale) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"sales"
		SET
			id_nomer = :id_nomer,
			scan_barcode = :scan_barcode,
			branch_id = :branch_id,
			shop_id = :shop_id,
			employee_id = :employee_id,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.GetId(),
		"id_nomer":     req.GetIdNomer(),
		"scan_barcode": req.GetScanBarcode(),
		"branch_id":    req.GetBranchId(),
		"shop_id":      req.GetShopId(),
		"employee_id":  req.GetEmployeeId(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *saleRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"sales"
		` + set + ` , updated_at = NOW()
			WHERE id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *saleRepo) Delete(ctx context.Context, req *kassa_service.SalePrimaryKey) error {
	query := `DELETE FROM "sales" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
