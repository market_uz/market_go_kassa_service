package postgres

import (
	"app/genproto/kassa_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type saleProductRepo struct {
	db *pgxpool.Pool
}

func NewSaleProductRepo(db *pgxpool.Pool) *saleProductRepo {
	return &saleProductRepo{
		db: db,
	}
}

func (c *saleProductRepo) Create(ctx context.Context, req *kassa_service.CreateSaleProduct) (resp *kassa_service.SaleProductPrimaryKey, err error) {
	var id = uuid.New().String()

	query := `
		INSERT INTO "sale_products" (
			id, 
			category_id,
			product_id,
			barcode,
			count,
			discount,
			discount_type,
			updated_at
		) VALUES ($1, $2, $3, $4, $5, $6, $7, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.CategoryId,
		req.ProductId,
		req.Barcode,
		req.Count,
		req.Discount,
		req.DiscountType,
	)
	if err != nil {
		return nil, err
	}

	return &kassa_service.SaleProductPrimaryKey{Id: id}, nil
}

func (c *saleProductRepo) GetByPKey(ctx context.Context, req *kassa_service.SaleProductPrimaryKey) (resp *kassa_service.SaleProduct, err error) {
	query := `
		SELECT
			id,
			category_id,
			product_id,
			barcode,
			count,
			discount,
			discount_type,
			price,
			total_price,
			created_at,
			updated_at
		FROM "sale_products"
		WHERE id = $1
	`

	var (
		id            sql.NullString
		category_id   sql.NullString
		product_id    sql.NullString
		barcode       sql.NullString
		count         sql.NullInt32
		discount      sql.NullFloat64
		discount_type sql.NullString
		price         sql.NullFloat64
		total_price   sql.NullFloat64
		created_at    sql.NullString
		updated_at    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&category_id,
		&product_id,
		&barcode,
		&count,
		&discount,
		&discount_type,
		&price,
		&total_price,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &kassa_service.SaleProduct{
		Id:           id.String,
		CategoryId:   category_id.String,
		ProductId:    product_id.String,
		Barcode:      barcode.String,
		Count:        count.Int32,
		Discount:     discount.Float64,
		DiscountType: discount_type.String,
		Price:        price.Float64,
		TotalPrice:   total_price.Float64,
		CreatedAt:    created_at.String,
		UpdatedAt:    updated_at.String,
	}

	return
}

func (c *saleProductRepo) GetAll(ctx context.Context, req *kassa_service.GetListSaleProductRequest) (resp *kassa_service.GetListSaleProductResponse, err error) {
	resp = &kassa_service.GetListSaleProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			category_id,
			product_id,
			barcode,
			count,
			discount,
			discount_type,
			price,
			total_price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sale_products"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id            sql.NullString
			category_id   sql.NullString
			product_id    sql.NullString
			barcode       sql.NullString
			count         sql.NullInt32
			discount      sql.NullFloat64
			discount_type sql.NullString
			price         sql.NullFloat64
			total_price   sql.NullFloat64
			created_at    sql.NullString
			updated_at    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&category_id,
			&product_id,
			&barcode,
			&count,
			&discount,
			&discount_type,
			&price,
			&total_price,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.SaleProducts = append(resp.SaleProducts, &kassa_service.SaleProduct{
			Id:           id.String,
			CategoryId:   category_id.String,
			ProductId:    product_id.String,
			Barcode:      barcode.String,
			Count:        count.Int32,
			Discount:     discount.Float64,
			DiscountType: discount_type.String,
			Price:        price.Float64,
			TotalPrice:   total_price.Float64,
			CreatedAt:    created_at.String,
			UpdatedAt:    updated_at.String,
		})
	}

	return
}

func (c *saleProductRepo) Update(ctx context.Context, req *kassa_service.UpdateSaleProduct) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"sale_products"
		SET
			sale_id = :sale_id,
			category = :category,
			product = :product,
			barcode = :barcode,
			count = :count,
			discount = :discount,
			discount_type = :discount_type,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":            req.GetId(),
		"sale_id":       req.GetSaleId(),
		"category":      req.GetCategoryId(),
		"product":       req.GetProductId(),
		"barcode":       req.GetBarcode(),
		"count":         req.GetCount(),
		"discount":      req.GetDiscount(),
		"discount_type": req.GetDiscountType(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *saleProductRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"sale_products"
		` + set + ` , updated_at = NOW()
			WHERE id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *saleProductRepo) Delete(ctx context.Context, req *kassa_service.SaleProductPrimaryKey) error {
	query := `DELETE FROM "sale_products" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
