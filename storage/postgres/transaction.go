package postgres

import (
	"app/genproto/kassa_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type transactionRepo struct {
	db *pgxpool.Pool
}

func NewTransactionRepo(db *pgxpool.Pool) *transactionRepo {
	return &transactionRepo{
		db: db,
	}
}

func (c *transactionRepo) Create(ctx context.Context, req *kassa_service.CreateTransaction) (resp *kassa_service.TransactionPrimaryKey, err error) {
	var id = uuid.New().String()

	query := `
		INSERT INTO "transactions" (
			id,
			smena_id
			cash,
			uzcard,
			apelsin,
			payme,
			visa,
			click,
			updated_at
		) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.SmenaId,
		req.Cash,
		req.Uzcard,
		req.Humo,
		req.Apelsin,
		req.Payme,
		req.Visa,
		req.Click,
	)
	if err != nil {
		return nil, err
	}

	return &kassa_service.TransactionPrimaryKey{Id: id}, nil
}

func (c *transactionRepo) GetByPKey(ctx context.Context, req *kassa_service.TransactionPrimaryKey) (resp *kassa_service.Transaction, err error) {
	query := `
		SELECT
			id,
			smena_id,
			cash,
			uzcard,
			humo,
			apelsin,
			payme,
			visa,
			click,
			total_sum,
			created_at,
			updated_at
		FROM "transactions"
		WHERE id = $1
	`

	var (
		id         sql.NullString
		smena_id   sql.NullString
		cash       sql.NullFloat64
		uzcard     sql.NullFloat64
		humo       sql.NullFloat64
		apelsin    sql.NullFloat64
		payme      sql.NullFloat64
		visa       sql.NullFloat64
		click      sql.NullFloat64
		total_sum  sql.NullFloat64
		created_at sql.NullString
		updated_at sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&smena_id,
		&cash,
		&uzcard,
		&humo,
		&apelsin,
		&payme,
		&visa,
		&click,
		&total_sum,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &kassa_service.Transaction{
		Id:        id.String,
		SmenaId:   smena_id.String,
		Cash:      cash.Float64,
		Uzcard:    uzcard.Float64,
		Humo:      humo.Float64,
		Apelsin:   apelsin.Float64,
		Payme:     payme.Float64,
		Visa:      visa.Float64,
		Click:     click.Float64,
		TotalSum:  total_sum.Float64,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}

	return
}

func (c *transactionRepo) GetAll(ctx context.Context, req *kassa_service.GetListTransactionRequest) (resp *kassa_service.GetListTransactionResponse, err error) {
	resp = &kassa_service.GetListTransactionResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			SELECT
			id,
			smena_id,
			cahs,
			uzcard,
			humo,
			apelsin,
			payme,
			visa,
			click,
			total_sum,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "transactions"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id         sql.NullString
			smena_id   sql.NullString
			cash       sql.NullFloat64
			uzcard     sql.NullFloat64
			humo       sql.NullFloat64
			apelsin    sql.NullFloat64
			payme      sql.NullFloat64
			visa       sql.NullFloat64
			click      sql.NullFloat64
			total_sum  sql.NullFloat64
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&smena_id,
			&cash,
			&uzcard,
			&humo,
			&apelsin,
			&payme,
			&visa,
			&click,
			&total_sum,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.Transactions = append(resp.Transactions, &kassa_service.Transaction{
			Id:        id.String,
			SmenaId:   smena_id.String,
			Cash:      cash.Float64,
			Uzcard:    uzcard.Float64,
			Humo:      humo.Float64,
			Apelsin:   apelsin.Float64,
			Payme:     payme.Float64,
			Visa:      visa.Float64,
			Click:     click.Float64,
			TotalSum:  total_sum.Float64,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}

	return
}

func (c *transactionRepo) Update(ctx context.Context, req *kassa_service.UpdateTransaction) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"transactions"
		SET
			smena_id = :smena_id,
			cash = :cash,
			uzcard = :humo,
			apelsin = :apelsin,
			payme = :payme,
			visa = :visa,
			click = :click,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":       req.GetId(),
		"smena_id": req.GetSmenaId(),
		"cash":     req.GetCash(),
		"uzcard":   req.GetUzcard(),
		"humo":     req.GetHumo(),
		"apelsin":  req.GetApelsin(),
		"payme":    req.GetPayme(),
		"visa":     req.GetVisa(),
		"click":    req.GetClick(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *transactionRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"transactions"
		` + set + ` , updated_at = NOW()
			WHERE id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *transactionRepo) Delete(ctx context.Context, req *kassa_service.TransactionPrimaryKey) error {
	query := `DELETE FROM "transactions" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
