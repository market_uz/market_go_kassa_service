package postgres

import (
	"app/config"
	"app/storage"
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Store struct {
	db          *pgxpool.Pool
	sale        storage.SaleRepoI
	saleProduct storage.SaleProductRepoI
	salePayment storage.SalePaymentRepoI
	transaction storage.TransactionRepoI
	valyuta     storage.ValyutaRepoI
	smena       storage.SmenaRepoI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Sale() storage.SaleRepoI {
	if s.sale == nil {
		s.sale = NewSaleRepo(s.db)
	}
	return s.sale
}

func (s *Store) SaleProduct() storage.SaleProductRepoI {
	if s.saleProduct == nil {
		s.saleProduct = NewSaleProductRepo(s.db)
	}
	return s.saleProduct
}

func (s *Store) SalePayment() storage.SalePaymentRepoI {
	if s.salePayment == nil {
		s.salePayment = NewSalePaymentRepo(s.db)
	}
	return s.salePayment
}

func (s *Store) Transaction() storage.TransactionRepoI {
	if s.transaction == nil {
		s.transaction = NewTransactionRepo(s.db)
	}
	return s.transaction
}

func (s *Store) Valyuta() storage.ValyutaRepoI {
	if s.valyuta == nil {
		s.valyuta = NewValyutaRepo(s.db)
	}
	return s.valyuta
}

func (s *Store) Smena() storage.SmenaRepoI {
	if s.smena == nil {
		s.smena = NewSmenaRepo(s.db)
	}
	return s.smena
}

func (l *Store) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	args := make([]interface{}, 0, len(data)+2) // making space for arguments + level + msg
	args = append(args, level, msg)
	for k, v := range data {
		args = append(args, fmt.Sprintf("%s=%v", k, v))
	}
	log.Println(args...)
}
