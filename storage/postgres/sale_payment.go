package postgres

import (
	"app/genproto/kassa_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type salePaymentRepo struct {
	db *pgxpool.Pool
}

func NewSalePaymentRepo(db *pgxpool.Pool) *salePaymentRepo {
	return &salePaymentRepo{
		db: db,
	}
}

func (c *salePaymentRepo) Create(ctx context.Context, req *kassa_service.CreateSalePayment) (resp *kassa_service.SalePaymentPrimaryKey, err error) {
	var id = uuid.New().String()

	query := `
		INSERT INTO "sale_payments" (
			id, 
			sale_id,
			cash,
			uzcard,
			humo,
			apelsin,
			payme,
			visa,
			click,
			currency_type,
			currency_price,
			updated_at
		) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.SaleId,
		req.Cash,
		req.Uzcard,
		req.Humo,
		req.Apelsin,
		req.Payme,
		req.Visa,
		req.Click,
		req.CurrencyType,
		req.CurrencyPrice,
	)
	if err != nil {
		return nil, err
	}

	return &kassa_service.SalePaymentPrimaryKey{Id: id}, nil
}

func (c *salePaymentRepo) GetByPKey(ctx context.Context, req *kassa_service.SalePaymentPrimaryKey) (resp *kassa_service.SalePayment, err error) {
	query := `
		SELECT
			id, 
			sale_id,
			cash,
			uzcard,
			humo,
			apelsin,
			payme,
			visa,
			click,
			currency_type,
			currency_price,
			created_at,
			updated_at
		FROM "sale_payments"
		WHERE id = $1
	`

	var (
		id             sql.NullString
		sale_id        sql.NullString
		cash           sql.NullFloat64
		uzcard         sql.NullFloat64
		humo           sql.NullFloat64
		apelsin        sql.NullFloat64
		payme          sql.NullFloat64
		visa           sql.NullFloat64
		click          sql.NullFloat64
		currency_type  sql.NullString
		currency_price sql.NullFloat64
		created_at     sql.NullString
		updated_at     sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&sale_id,
		&cash,
		&uzcard,
		&humo,
		&apelsin,
		&payme,
		&visa,
		&click,
		&currency_type,
		currency_price,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &kassa_service.SalePayment{
		Id:            id.String,
		SaleId:        sale_id.String,
		Cash:          cash.Float64,
		Uzcard:        uzcard.Float64,
		Humo:          humo.Float64,
		Apelsin:       apelsin.Float64,
		Payme:         payme.Float64,
		Visa:          visa.Float64,
		Click:         click.Float64,
		CurrencyType:  currency_type.String,
		CurrencyPrice: currency_price.Float64,
		CreatedAt:     created_at.String,
		UpdatedAt:     updated_at.String,
	}

	return
}

func (c *salePaymentRepo) GetAll(ctx context.Context, req *kassa_service.GetListSalePaymentRequest) (resp *kassa_service.GetListSalePaymentResponse, err error) {
	resp = &kassa_service.GetListSalePaymentResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			sale_id,
			cash,
			uzcard,
			humo,
			apelsin,
			payme,
			visa,
			click,
			currency_type,
			currency_price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sale_payments"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id             sql.NullString
			sale_id        sql.NullString
			cash           sql.NullFloat64
			uzcard         sql.NullFloat64
			humo           sql.NullFloat64
			apelsin        sql.NullFloat64
			payme          sql.NullFloat64
			visa           sql.NullFloat64
			click          sql.NullFloat64
			currency_type  sql.NullString
			currency_price sql.NullFloat64
			created_at     sql.NullString
			updated_at     sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&sale_id,
			&cash,
			&uzcard,
			&humo,
			&apelsin,
			&payme,
			&visa,
			&click,
			&currency_type,
			currency_price,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.SalePayments = append(resp.SalePayments, &kassa_service.SalePayment{
			Id:            id.String,
			SaleId:        sale_id.String,
			Cash:          cash.Float64,
			Uzcard:        uzcard.Float64,
			Humo:          humo.Float64,
			Apelsin:       apelsin.Float64,
			Payme:         payme.Float64,
			Visa:          visa.Float64,
			Click:         click.Float64,
			CurrencyType:  currency_type.String,
			CurrencyPrice: currency_price.Float64,
			CreatedAt:     created_at.String,
			UpdatedAt:     updated_at.String,
		})
	}

	return
}

func (c *salePaymentRepo) Update(ctx context.Context, req *kassa_service.UpdateSalePayment) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"sale_payments"
		SET
			sale_id = :sale_id,
			cash = :cash,
			uzcard = :uzcard,
			humo = :humo,
			apelsin = :apelsin,
			payme = :payme,
			visa = :visa,
			click = :click,
			currency_type,
			currency_price,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":             req.GetId(),
		"sale_id":        req.GetSaleId(),
		"cash":           req.GetCash(),
		"uzcard":         req.GetUzcard(),
		"humo":           req.GetHumo(),
		"apelsin":        req.GetApelsin(),
		"payme":          req.GetPayme(),
		"visa":           req.GetVisa(),
		"click":          req.GetClick(),
		"currency_type":  req.GetCurrencyType(),
		"currency_price": req.GetCurrencyPrice(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *salePaymentRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"sale_payments"
		` + set + ` , updated_at = NOW()
			WHERE id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *salePaymentRepo) Delete(ctx context.Context, req *kassa_service.SalePaymentPrimaryKey) error {
	query := `DELETE FROM "sale_payments" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
