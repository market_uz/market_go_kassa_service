package postgres

import (
	"app/genproto/kassa_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type smenaRepo struct {
	db *pgxpool.Pool
}

func NewSmenaRepo(db *pgxpool.Pool) *smenaRepo {
	return &smenaRepo{
		db: db,
	}
}

func (c *smenaRepo) Create(ctx context.Context, req *kassa_service.CreateSmena) (resp *kassa_service.SmenaPrimaryKey, err error) {
	var id = uuid.New().String()

	id_nomer := helper.GenerateCode()

	query := `
		INSERT INTO "smenas" (
			id, 
			id_nomer,
			employee_id,
			branch_id,
			shop_id,
			updated_at
		) VALUES ($1, $2, $3, $4, $5, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		id_nomer,
		req.EmployeeId,
		req.BranchId,
		req.ShopId,
	)
	if err != nil {
		return nil, err
	}

	return &kassa_service.SmenaPrimaryKey{Id: id}, nil
}

func (c *smenaRepo) GetByPKey(ctx context.Context, req *kassa_service.SmenaPrimaryKey) (resp *kassa_service.Smena, err error) {
	query := `
		SELECT
			id,
			id_nomer,
			employee_id,
			branch_id,
			shop_id,
			status,
			created_at,
			updated_at
		FROM "smenas"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		id_nomer    sql.NullString
		employee_id sql.NullString
		branch_id   sql.NullString
		shop_id     sql.NullString
		status      sql.NullInt32
		created_at  sql.NullString
		updated_at  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&id_nomer,
		&employee_id,
		&branch_id,
		&shop_id,
		&status,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &kassa_service.Smena{
		Id:         id.String,
		IdNomer:    id_nomer.String,
		EmployeeId: employee_id.String,
		BranchId:   branch_id.String,
		ShopId:     shop_id.String,
		Status:     status.Int32,
		CreatedAt:  created_at.String,
		UpdatedAt:  updated_at.String,
	}

	return
}

func (c *smenaRepo) GetAll(ctx context.Context, req *kassa_service.GetListSmenaRequest) (resp *kassa_service.GetListSmenaResponse, err error) {
	resp = &kassa_service.GetListSmenaResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			id_nomer,
			employee_id,
			branch_id,
			shop_id,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sales"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id          sql.NullString
			id_nomer    sql.NullString
			employee_id sql.NullString
			branch_id   sql.NullString
			shop_id     sql.NullString
			status      sql.NullInt32
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&id_nomer,
			&employee_id,
			&branch_id,
			&shop_id,
			&status,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.Smenas = append(resp.Smenas, &kassa_service.Smena{
			Id:         id.String,
			IdNomer:    id_nomer.String,
			EmployeeId: employee_id.String,
			BranchId:   branch_id.String,
			ShopId:     shop_id.String,
			Status:     status.Int32,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}

	return
}

func (c *smenaRepo) Update(ctx context.Context, req *kassa_service.UpdateSmena) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"smenas"
		SET
			id_nomer = :id_nomer,
			branch_id = :branch_id,
			shop_id = :shop_id,
			employee_id = :employee_id,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.GetId(),
		"id_nomer":    req.GetIdNomer(),
		"branch_id":   req.GetBranchId(),
		"shop_id":     req.GetShopId(),
		"employee_id": req.GetEmployeeId(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *smenaRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"smenas"
		` + set + ` , updated_at = NOW()
			WHERE id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *smenaRepo) Delete(ctx context.Context, req *kassa_service.SmenaPrimaryKey) error {
	query := `DELETE FROM "smenas" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
