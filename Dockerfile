
FROM golang:alpine

RUN mkdir myapp

ADD . /myapp

WORKDIR /myapp

RUN go mod tidy
RUN go build -o main cmd/main.go

CMD ./main

