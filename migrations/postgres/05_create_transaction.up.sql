CREATE TABLE IF NOT EXISTS "transactions" (
    "id" UUID PRIMARY KEY,
    "smena_id" UUID REFERENCES smenas(id),
    "cash" NUMERIC DEFAULT 0,
    "uzcard" NUMERIC DEFAULT 0,
    "humo" NUMERIC DEFAULT 0,
    "apelsin" NUMERIC DEFAULT 0,
    "payme" NUMERIC DEFAULT 0,
    "visa" NUMERIC DEFAULT 0,
    "click" NUMERIC DEFAULT 0,
    "total_sum" NUMERIC DEFAULT 0,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);