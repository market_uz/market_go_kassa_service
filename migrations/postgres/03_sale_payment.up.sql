CREATE TABLE IF NOT EXISTS "sale_payments" (
    "id" UUID PRIMARY KEY,
    "sale_id" UUID REFERENCES sales(id),
    "cash" NUMERIC,
    "uzcard" NUMERIC,
    "humo" NUMERIC,
    "apelsin" NUMERIC,
    "payme" NUMERIC,
    "visa" NUMERIC,
    "click" NUMERIC,
    "currency_type" VARCHAR,
    "currency_price" NUMERIC,
    "total_sum" NUMERIC,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP 
);