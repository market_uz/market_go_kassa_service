CREATE TABLE IF NOT EXISTS "currency_data" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    "abreviatura" VARCHAR NOT NULL,
    "price" NUMERIC DEFAULT 0,
    "date" TIMESTAMP,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);