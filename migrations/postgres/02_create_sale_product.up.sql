CREATE TABLE IF NOT EXISTS "sale_products" (
    "id" UUID PRIMARY KEY,
    "sale_id" UUID REFERENCES sales(id),
    "category_id" UUID NOT NULL,
    "product_id" UUID NOT NULL,
    "barcode" VARCHAR NOT NULL,
    "count" INT NOT NULL,
    "discount" NUMERIC DEFAULT 0,
    "discount_type" VARCHAR,
    "price" NUMERIC,
    "total_price" NUMERIC,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP 
);
