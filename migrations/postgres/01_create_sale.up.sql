CREATE TABLE IF NOT EXISTS "sales" (
    "id" UUID PRIMARY KEY,
    "scan_barcode" VARCHAR NOT NULL,
    "id_nomer" VARCHAR NOT NULL,
    "branch_id" UUID NOT NULL,
    "shop_id" UUID NOT NULL,
    "employee_id" UUID NOT NULL,
    -- 1 - IN_PROCESS, 2 - FINISHED
    "status" SMALLINT DEFAULT 1,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP 
);
