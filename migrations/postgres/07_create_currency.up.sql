CREATE TABLE IF NOT EXISTS "currency" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    "abreviatura" VARCHAR NOT NULL
);