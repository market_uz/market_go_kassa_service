CREATE TABLE IF NOT EXISTS "smenas" (
    "id" UUID PRIMARY KEY,
    "id_nomer" VARCHAR NOT NULL,
    "employee_id" UUID NOT NULL,
    "branch_id" UUID NOT NULL,
    "shop_id" UUID NOT NULL,
    -- 1 - OPEN, 2 - FINISHED
    "status" SMALLINT DEFAULT 1,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);